// JSON Objects
/*
- It stands for JavaScript Object Notation
- Also used in other programming languages
- Core Javascript has a built in JSON object that contains methods for parsing JSON Objects and converting strings into Javascript objects
- Javascript Objects are not to be confused with JSON
- Serialization is the process of converting data into a series of bytes for easier transmission / transfer of information
- A byte is a unit of data that is eight binary digits (1 and 0) that is used to represent a character (letters, numbers, or typographic symbols)
- Syntax:
    {
        "propertyA": "propertyA",
        "propertyB": "propertyB"
    }
*/

// JSON
// {
//     "city": "Quezon City",
//     "province": "Metro Manila",
//     "country": "Philippines"
// }

// JSON, Array
// "cities": [
//     {"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
//     {"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
//     {"city": "Makati City", "province": "Metro Manila", "country": "Philippines"},
// ]

// PARSE: Converting JSON string into objects
let batchesJSON = '[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]';
console.log('Result from parse method:');
console.log(JSON.parse(batchesJSON));
// console.log(batchesJSON[0].batchName);
let stringifiedObject = `{"name": "Eric", "age": "9", "address": {"city": "Bonifacio Global City", "country": "Philippines"}}`
console.log(JSON.parse(stringifiedObject));

// STRINGIFY: Convert Objects into String(JSON)
// JSON.stringy()
let data = {
    name: "Gabryl",
    age: 61,
    address: {
        city: "New York",
        country: "USA"
    }
}